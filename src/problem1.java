import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
//problem - 1 Number of matches played per year of all the years in IPL.
public class problem1 {
    public static void main(String[] args) {

        HashMap<String , Integer> map = new HashMap<>();
        BufferedReader br = null;
        String line = "";
        int skip = 0;

        try {
            br = new BufferedReader(new FileReader("/home/vansh/Desktop/java/IPL_PRO/src/Data/matches.csv"));

            while((line = br.readLine()) != null) {
                if (skip == 0) {
                    ++skip;
                }
                else {
                    String[] match = line.split(",");

                    if(map.containsKey(match[1])){
                        map.put(match[1], map.get(match[1])+1);
                    }
                    else{
                        map.put(match[1],1);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Matches played per year : " + map);
    }
}
