import com.sun.source.tree.Tree;

import java.io.*;
import java.util.*;

public class problem4 {

    public static void main(String[] args) {

        Map<String, Float> bowlerRan = new TreeMap<>();
        Map<String,Float> bowlerOver = new TreeMap<>();
        Map<String,Float> result = new TreeMap<>();

        HashMap<String,Integer> store = new HashMap<>();

        BufferedReader br1 = null;
        BufferedReader br2 = null;
        String line1 = "";
        String line2 = "";
        int skip = 0;
        int x = 0;
        int matchId1 = 0;

        try {
            br1 = new BufferedReader(new FileReader("/home/vansh/Desktop/java/IPL_PRO/src/Data/matches.csv"));
            while((line1 = br1.readLine()) != null) {
                if (skip == 0) {
                    ++skip;
                } else {
                    String[] match = line1.split(",");
                    store.put(match[1], Integer.parseInt(match[0]));
                    if(Objects.equals(match[1], "2015")){
                        matchId1++;
                    }
                }
            }
            int matchId2 = store.get("2015");
            matchId1 = matchId2 - matchId1+1;

            br2 = new BufferedReader(new FileReader("/home/vansh/Desktop/java/IPL_PRO/src/Data/deliveries.csv"));
            while((line2 = br2.readLine()) != null) {
                if (x == 0) {
                    ++x;
                } else {
                    String[] deliveries = line2.split(",");
                    int id = (Integer.parseInt(deliveries[0]));

                    if(id >= matchId1 && id <= matchId2) {
                        if (bowlerRan.containsKey(deliveries[8])) {
                            bowlerRan.put(deliveries[8], bowlerRan.get(deliveries[8]) + Integer.parseInt(deliveries[17]));
                        } else {
                            bowlerRan.put(deliveries[8], 0.f);
                        }
                        if(bowlerOver.containsKey(deliveries[8])){
                            bowlerOver.put(deliveries[8],bowlerOver.get(deliveries[8])+(int)(Integer.parseInt(deliveries[5])/6));
                        }
                        else{
                            bowlerOver.put(deliveries[8],0.0f);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(Map.Entry<String,Float> entry : bowlerRan.entrySet()){
            result.put(entry.getKey(),(bowlerRan.get(entry.getKey())/bowlerOver.get(entry.getKey())));
        }
;
        System.out.println(result);

    }
}

