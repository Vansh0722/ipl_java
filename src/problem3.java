import java.io.*;
import java.util.*;
//Problem-3 For the year 2016 get the extra runs conceded per team.
public class problem3 {
    public static void main(String[] args) {

        HashMap<String,Integer> map1 = new HashMap<>();
        HashMap<String,Integer> map2 = new HashMap<>();

        BufferedReader br1 = null;
        BufferedReader br2 = null;

        String line1 = "";
        String line2 = "";
        int skip1 = 0;
        int skip2 = 0;
        int matchId = 0;

        try {
            br1 = new BufferedReader(new FileReader("/home/vansh/Desktop/java/IPL_PRO/src/Data/matches.csv"));

            while((line1 = br1.readLine()) != null){
                if (skip1 == 0) {
                    ++skip1;
                }
                else {
                    String[] match = line1.split(",");
                    map1.put(match[1], Integer.parseInt(match[0]));
                    if(Objects.equals(match[1], "2016")){
                        matchId++;
                    }
                }
            }
            int matchId2 = map1.get("2016");
            matchId = matchId2 - matchId+1;

            br2 = new BufferedReader(new FileReader("/home/vansh/Desktop/java/IPL_PRO/src/Data/deliveries.csv"));

            while((line2 = br2.readLine()) != null) {
                if (skip2 == 0) {
                    ++skip2;
                } else {
                    String[] match = line2.split(",");

                    if(Integer.parseInt(match[0]) >= matchId && Integer.parseInt(match[0])<=matchId2){
                        if(map2.containsKey(match[3])){
                            map2.put(match[3],map2.get(match[3])+Integer.parseInt(match[16]));
                        }
                        else{
                            map2.put(match[3], 0);
                        }
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Extra runs per team in 2016 : " + map2);
    }
}
