
import java.io.*;
import java.util.*;

public class Main {
    static final String csvSplitBy = ",";
    static final String csvFileMatches = "/home/vansh/Desktop/java/IPL_PRO/src/Data/matches.csv";
    static final String csvFileDeliveries = "/home/vansh/Desktop/java/IPL_PRO/src/Data/deliveries.csv";
    static String line="";

    public static void main(String[] args) {
//        Main pro1 = new Main();


        List<Match> everyMatchData = getMatchesData();
        List<Deliveries> everyDeliveriesData = getDeliveriesData();
//        numberOfMatchPlayedPerYear(everyMatchData);
//        numberOfMatchesWonByTeam(everyMatchData);
//        extraRunsPerTeamInYear2016(everyDeliveriesData,everyMatchData);
        economicalBowlerForYear2015(everyDeliveriesData,everyMatchData);
    }

   public static List<Match> getMatchesData() {
        List<Match> matchesData = new ArrayList();

        BufferedReader br = null;
        int skip = 0;
        try {
            br = new BufferedReader(new FileReader(csvFileMatches));

            while ((line = br.readLine()) != null) {

                if (skip == 0) {
                    skip++;
                    continue;
                } else {
                    String[] eachLineData = line.split(csvSplitBy);
                    Match match = new Match();
                    match.setId(Integer.parseInt(eachLineData[0]));
                    match.setSeason(Integer.parseInt(eachLineData[1]));
                    match.setCity(eachLineData[2]);
                    match.setDate(eachLineData[3]);
                    match.setTeam1(eachLineData[4]);
                    match.setTeam2(eachLineData[5]);
                    match.setTossWinner(eachLineData[6]);
                    match.setTossDecision(eachLineData[7]);
                    match.setResult(eachLineData[8]);
                    match.setDlApplied(eachLineData[9]);
                    match.setWinner(eachLineData[10]);
                    match.setWinByRuns(Integer.parseInt(eachLineData[11]));
                    match.setWinByWickets(Integer.parseInt(eachLineData[12]));
                    match.setPlayerOfMatch(eachLineData[13]);
                    match.setVenue(eachLineData[14]);
                    matchesData.add(match);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matchesData;

    }

    public static List<Deliveries> getDeliveriesData() {
        List<Deliveries> deliveriesData = new ArrayList<>();

        BufferedReader br = null;
        int skip = 0;

        try {
            br = new BufferedReader(new FileReader(csvFileDeliveries));
            while ((line = br.readLine()) != null) {
                if (skip == 0) {
                    skip++;
                    continue;
                }
                String[] eachLineData = line.split(csvSplitBy);
                Deliveries deliveries = new Deliveries();

                deliveries.setMatchId(Integer.parseInt(eachLineData[0]));
                deliveries.setInning(Integer.parseInt(eachLineData[1]));
                deliveries.setBattingTeam(eachLineData[2]);
                deliveries.setBowlingTeam(eachLineData[3]);
                deliveries.setOver(Integer.parseInt(eachLineData[4]));
                deliveries.setBall(Integer.parseInt(eachLineData[5]));
                deliveries.setBatsman(eachLineData[6]);
                deliveries.setNonStriker(eachLineData[7]);
                deliveries.setBowler(eachLineData[8]);
                deliveries.setIsSuperOver(Integer.parseInt(eachLineData[9]));
                deliveries.setWideRuns(Integer.parseInt(eachLineData[10]));
                deliveries.setByeRuns(Integer.parseInt(eachLineData[11]));
                deliveries.setLegByeRuns(Integer.parseInt(eachLineData[12]));
                deliveries.setNoballRuns(Integer.parseInt(eachLineData[13]));
                deliveries.setPenaltyRuns(Integer.parseInt(eachLineData[14]));
                deliveries.setBatsmanRuns(Integer.parseInt(eachLineData[15]));
                deliveries.setExtraRuns(Integer.parseInt(eachLineData[16]));
                deliveries.setTotalRuns(Integer.parseInt(eachLineData[17]));
                deliveriesData.add(deliveries);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deliveriesData;
    }


    //problem - 1 Number of matches played per year of all the years in IPL.
    public static Map<Integer, Integer> numberOfMatchPlayedPerYear(List<Match> everyMatchData) {
        Map<Integer, Integer> totalMatches = new TreeMap<>();
        for (Match match:  everyMatchData) {

            Integer year = match.getSeason();
            if(totalMatches.containsKey(year)){
                totalMatches.put(year,(totalMatches.get(year) +1));
            }
            else {
                totalMatches.put(year,1);
            }
        }
        System.out.println(totalMatches);
        return totalMatches;
    }


//     problem - 2 Number of matches won of all teams over all the years of IPL.
    public static Map<String ,Integer> numberOfMatchesWonByTeam(List<Match> everyMatchData) {
        TreeMap<String,Integer> matchesWonByTeam = new TreeMap<>();

        for (Match match: everyMatchData ){
            String winner =match.getWinner();
            if(winner.isBlank()){
                    continue;
                }
                if(matchesWonByTeam.containsKey(winner)) {
                    matchesWonByTeam.put(winner, matchesWonByTeam.get(winner) + 1);
                }
                else {
                    matchesWonByTeam.put(winner, 1);
                }}
        System.out.println("Matches won per team : " + matchesWonByTeam);
        return matchesWonByTeam;
    }


    //Problem-3 For the year 2016 get the extra runs conceded per team.
    public static Map<String ,Integer> extraRunsPerTeamInYear2016(List<Deliveries> deliveries, List<Match> matches){
        Map<Integer,Integer> map1 = new TreeMap<>();
        Map<String,Integer> map2 = new TreeMap<>();
        int matchId = 0;
        int matchId2=0;
        for (Match match : matches) {
                    map1.put(match.getSeason(), match.getId());
                    if(match.getSeason()==2016){
                        matchId++;
                    }
                    matchId2 = match.getId();
                }
            matchId = matchId2 - matchId+1;

        for (Deliveries delivery : deliveries)  {
                    if( delivery.getMatchId()>= matchId && delivery.getMatchId()<=matchId2){
                        if(map2.containsKey(delivery.getBowlingTeam())){
                            map2.put(delivery.getBowlingTeam(),map2.get(delivery.getBowlingTeam())+delivery.getExtraRuns());
                        }
                        else{
                            map2.put(delivery.getBowlingTeam(), 0);
                        }
                    }
                }
        System.out.println("Extra runs per team in 2016 : " + map2);
        return  map2;
    }


    //Problem - 4. For the year 2015 get the top economical bowlers.
    public static Map<String,Integer> economicalBowlerForYear2015(List<Deliveries> deliveries, List<Match> matches) {
        TreeMap<String, Integer> runsGivenByBowler2015 = new TreeMap<>();
        TreeMap<String, Integer> totalOverByBowler2015 = new TreeMap<>();
        TreeMap<String, Integer> economyPerBowler = new TreeMap<>();

        int firstMatchId2015;
        int lastMatchId2015 = 0;
        int counterForMatch2015 = 0;

        for (Match match : matches) {
            if (match.getSeason() == 2015) {
                counterForMatch2015++;
                lastMatchId2015 = match.getId();
            }
        }
        firstMatchId2015 = lastMatchId2015 - counterForMatch2015 + 1;

        for (Deliveries delivery : deliveries) {

            if (delivery.getMatchId() >= firstMatchId2015 && delivery.getMatchId() <= lastMatchId2015) {
                if (runsGivenByBowler2015.containsKey(delivery.getBowler())) {
                    runsGivenByBowler2015.put(delivery.getBowler(), runsGivenByBowler2015.get(delivery.getBowler()) + delivery.getTotalRuns());
                } else {
                    runsGivenByBowler2015.put(delivery.getBowler(), delivery.getTotalRuns());
                }
                if (totalOverByBowler2015.containsKey(delivery.getBowler())) {
                    totalOverByBowler2015.put(delivery.getBowler(), totalOverByBowler2015.get(delivery.getBowler()) + delivery.getBall() / 6);
                } else {
                    totalOverByBowler2015.put(delivery.getBowler(), 0);
                }
            }
        }
        for(Map.Entry<String,Integer> entry : runsGivenByBowler2015.entrySet()) {

            economyPerBowler.put(entry.getKey(), (runsGivenByBowler2015.get(entry.getKey()) / totalOverByBowler2015.get(entry.getKey())));
        }
        System.out.println(economyPerBowler);
        return economyPerBowler;
    }
}
