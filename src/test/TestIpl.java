import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import static org.junit.jupiter.api.Assertions.assertTrue;



public class TestIpl {
    Main main = new Main();

    List<Match> everyMatchData = main.getMatchesData();
    List<Deliveries> everyDeliveriesData = main.getDeliveriesData();
    Map<Integer, Integer> matchesCount = new TreeMap<>();
    Map<String, Integer> winnerCount = new TreeMap<>();
    Map<String, Integer> extraRunsPerTeams = new TreeMap<>();
    Map<String, Integer> bowlersAndEconomy = main.economicalBowlerForYear2015(everyDeliveriesData, everyMatchData);


    @Nested
    class MatchesPlayedYearly {
        @BeforeEach
        void matchesPlayedPerYear() {
            matchesCount.put(2009, 57);
            matchesCount.put(2008, 58);
            matchesCount.put(2017, 59);
            matchesCount.put(2016, 60);
            matchesCount.put(2015, 59);
            matchesCount.put(2014, 60);
            matchesCount.put(2013, 76);
            matchesCount.put(2012, 74);
            matchesCount.put(2011, 73);
            matchesCount.put(2010, 60);
        }

        @Test
        void matchesPlayedPerYearTest1() throws IOException {
            Assertions.assertEquals(matchesCount, main.numberOfMatchPlayedPerYear(main.getMatchesData()));
        }

        @Test
        void matchesPlayedPerYearTest2() throws IOException {
            assertTrue(matchesCount.equals(main.numberOfMatchPlayedPerYear(main.getMatchesData())));
        }

        @Test
        void matchesPlayedPerYearTest3() throws IOException {
            Assertions.assertNotNull(main.numberOfMatchPlayedPerYear(main.getMatchesData()));
        }
    }


    @Nested
    class MatchesWon {
        @BeforeEach
        @Test
        void matchesWonByTeam() {
            winnerCount.put("Mumbai Indians", 92);
            winnerCount.put("Sunrisers Hyderabad", 42);
            winnerCount.put("Pune Warriors", 12);
            winnerCount.put("Rajasthan Royals", 63);
            winnerCount.put("Kolkata Knight Riders", 77);
            winnerCount.put("Royal Challengers Bangalore", 73);
            winnerCount.put("Gujarat Lions", 13);
            winnerCount.put("Rising Pune Supergiant", 10);
            winnerCount.put("Kochi Tuskers Kerala", 6);
            winnerCount.put("Kings XI Punjab", 70);
            winnerCount.put("Deccan Chargers", 29);
            winnerCount.put("Delhi Daredevils", 62);
            winnerCount.put("Rising Pune Supergiants", 5);
            winnerCount.put("Chennai Super Kings", 79);
        }

        @Test
        void matchesWonByTeamTest1() {
            Assertions.assertEquals(winnerCount, main.numberOfMatchesWonByTeam(main.getMatchesData()));
        }

        @Test
        void matchesWonByTeamTest2() {
            Assertions.assertTrue(winnerCount.equals(main.numberOfMatchesWonByTeam(main.getMatchesData())));
        }

        @Test
        void matchesWonByTeamTest3() {
            Assertions.assertNotNull(main.numberOfMatchesWonByTeam(main.getMatchesData()));
        }
    }

//

    @Nested
    class ExtraRuns {
        @BeforeEach
        @Test
        void ExtraRunsIn2016() {
            extraRunsPerTeams.put("Gujarat Lions", 98);
            extraRunsPerTeams.put("Mumbai Indians", 102);
            extraRunsPerTeams.put("Sunrisers Hyderabad", 107);
            extraRunsPerTeams.put("Kings XI Punjab", 100);
            extraRunsPerTeams.put("Delhi Daredevils", 106);
            extraRunsPerTeams.put("Rising Pune Supergiants", 108);
            extraRunsPerTeams.put("Kolkata Knight Riders", 122);
            extraRunsPerTeams.put("Royal Challengers Bangalore", 151);
        }

        @Test
        void extraRunsPerTeamIn2016Test1() {
            Assertions.assertEquals(extraRunsPerTeams, main.extraRunsPerTeamInYear2016(main.getDeliveriesData(), main.getMatchesData() ));
        }

        @Test
        void extraRunsPerTeamIn2016Test2() {
            Assertions.assertTrue(extraRunsPerTeams.equals(main.extraRunsPerTeamInYear2016(main.getDeliveriesData(), main.getMatchesData())));
        }

        @Test
        void extraRunsPerTeamIn2016Test3() {
            Assertions.assertNotNull(main.extraRunsPerTeamInYear2016(main.getDeliveriesData(), main.getMatchesData()));
        }
    }



    @Nested
    class Economy{
            @Test
            void economyRatePerBowlerIn2015Test1() {
                Assertions.assertEquals(bowlersAndEconomy,main.economicalBowlerForYear2015(everyDeliveriesData, everyMatchData) );
            }

            @Test
            void economyRatePerBowlerIn2015Test2() {
                Assertions.assertTrue(bowlersAndEconomy.equals(main.economicalBowlerForYear2015(everyDeliveriesData, everyMatchData)));
            }

            @Test
            void economyRatePerBowlerIn2015Test3() {
                Assertions.assertNotNull(main.economicalBowlerForYear2015(everyDeliveriesData, everyMatchData));
            }
        }

}











